import Game.textual2048.textual_2048 as game
import IAs.random_IA as ia_1
import IAs.IA_opti as ia_2
import glob
import glob
import os
import importlib

os.chdir("..")

def faire_jouer(ia,game) :
    return(game.game_play(ia,afficher=False))

def load_ias():
    filenames = glob.glob('IAs/*.py')
    ias = list(map(lambda s: s[:-3].replace('/', '.'), filenames))
    ias = list(filter(lambda x : not("__" in x), ias))
    return ias

def scores():
    ias = load_ias()
    scores = []
    for ia in ias :
        mod_ia = importlib.import_module(ia)
        score, grille_finale, history = faire_jouer(mod_ia, game)
        scores.append([score, mod_ia.pseudo, grille_finale, history])

    return scores

def classement(scores):
    scores.sort(key = lambda x : x[0], reverse = True)
    for i, inst in enumerate(scores) :
        exp = "ème"
        if i == 0:
            exp = "er"
        elif i == 1:
            exp = "nd"
        score, pseudo, grille_finale, hist = inst
        print(str(i+1)+exp+" : ", pseudo, " avec un score de ", score)
    return scores

def tournoi():
    return classement(scores())