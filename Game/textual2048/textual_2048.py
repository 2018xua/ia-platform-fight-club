import Game.game2048.grid_2048 as game
import random as rd
import itertools
import os

def flatten(grid):
    flat = []
    for line in grid :
        flat += line
    return flat

def read_player_command(joueur,grid):
    commandes_valides = ["g", "d", "b", "h"]
    directions = {"g":"left", "d":"right", "b":"down", "h":"up"}
    move = joueur.play(grid)
    while move not in commandes_valides :
        move = joueur.play(grid)
    return directions[move]

def read_size_grid():
    eff = False
    while not(eff):
        try :
            n= int(input("Taille de la grille : "))
            eff = True
        except:
            print("Sous forme d'un entier !")
    return n

def read_theme_grid():
    eff = False
    while not(eff):
        try :
            n = int(input("Numero du theme"))
            if n<2:
                eff = True
        except:
            print("Sous forme d'un entier pus petit que 2!")
    return str(n)


def move_row_left(row_init):
    row = row_init[::]
    n = len(row)
    curs = 0
    nb_traite = 0
    deja_merge = False #indic si des tiles ont été merge précédemment pour éviter d'en merge plusieurs fois
    while nb_traite <n:
        nb_traite+=1
        if row[curs] == 0: #cas ou le tiles est un 0
            for j in range(curs, n-1):
                row[j] = row[j+1]  #on décale tout a gauche
            row[-1]=0 #création d'un nouveau 0
        elif curs>0 and row[curs] == row[curs-1] and not(deja_merge): #si on a deux égaux
            row[curs-1] *=2
            for j in range(curs, n-1):
                row[j] = row[j+1]
            row[-1]=0
            deja_merge = True #on indique qu'il a été merge
        else : #quand il y a rien de spécial
            curs+=1
            deja_merge = False
    return row


def move_row_right(row):
    return move_row_left(row[::-1])[::-1]



def transpo(m): #transpose une matrice
    n = len(m)
    res = [[0 for i in range(n)] for j in range(n)]
    for i in range(n):
        for j in range(n):
            res[i][j] = m[j][i]
    return (res)


def move_grid(m, sens):
    res = []
    if sens == "left":
        for l in m:
            res.append(move_row_left(l))
        return res

    elif sens == "right":
        for l in m:
            res.append(move_row_right(l))
        return res

    elif sens == "up":
        for l in transpo(m):
            res.append(move_row_left(l)) #aller en haut c'est comme aller a gauche sur la transposé puis retransposer
        return transpo(res)

    elif sens == "down":
        for l in transpo(m):
            res.append(move_row_right(l)) #pareil mais allelr en bas c'est pareil qu'aller a droite avec la transposée
        return transpo(res)

def is_grid_full(grid):
    return not(len(game.get_empty_tiles_positions(grid)))

def move_possible(grid):
    rep = [True for i in range(4)]
    i = 0
    for direction in ["left", "right", "up", "down"]:
        if move_grid(grid, direction) == grid:  #si la grille est la meme avant et apres le déplacement
            rep[i]=False
        i+=1
    return rep

def is_game_over(grid):
    return is_grid_full(grid) and not(any(move_possible(grid))) #si y a plus de mvmt possible

def get_grid_tile_max(grid):
    return max(max(l) for l in grid)

def random_play(grid = [], n=4, display = True):
    if not(grid):
        grid = game.create_grid(n)

        grid = [[game.grid_get_value(grid, i, j) for j in range(n)] for i in range(n)]
        for i in range(2):
            grid = game.grid_add_new_tile(grid)
    n = len(grid)
    if display :
        print(game.grid_to_string(grid, n))
    moves = ["left", "right", "up", "down"]
    while not(is_game_over(grid)):
        moves_selection = list(itertools.compress(moves, move_possible(grid)))
        if len(moves_selection)>0:
            direction = rd.choice(moves_selection) #prend une direction au hasard
            grid = move_grid(grid, direction)
        if len(game.get_empty_tiles_positions(grid)) >= 1:
            grid = game.grid_add_new_tile(grid)
        if display:
            print(game.grid_to_string(grid, n))
    #print(get_grid_tile_max(grid))
    return get_grid_tile_max(grid)

def game_play(joueur,afficher=True, n = 4, theme = "0"):
    history = []
    grid = game.create_grid(n)
    grid = [[game.grid_get_value(grid, i, j) for j in range(n)] for i in range(n)]
    for i in range(2):
        grid = game.grid_add_new_tile(grid) #initie le je ajoute 2 tiles

    if afficher :
        print(game.grid_to_string_with_size_and_theme(grid, game.THEMES[theme], n))
    history.append(flatten(grid))
    moves = ["left", "right", "up", "down"]
    while not(is_game_over(grid)):
        moves_selection = []
        for i in range(4):
            if move_possible(grid)[i]:
                moves_selection.append(moves[i])
        direction = "pas definie"
        if len(moves_selection)>0 :
            while not(direction in moves_selection):
                direction = read_player_command(joueur,grid) #le joueur choisie sa direction
            grid = move_grid(grid, direction) #ca joue
        if len(game.get_empty_tiles_positions(grid)) >= 1:
            grid = game.grid_add_new_tile(grid) #nouvelle tiles
        if afficher :
            print(game.grid_to_string_with_size_and_theme(grid, game.THEMES[theme], n))
        history.append(flatten(grid))

    return score(grid), grid, history

def score(grid):
    return sum(sum(l) for l in grid)


