import random as rd

def create_grid(n=4):
    """ Creates a square grid filed with " "s of size n"""
    return [[" " for i in range(n)]for j in range(n)]


def get_value_new_tile():
    """ Returns 4 with a probability of 0.1 and 2 with a probability of 0.9"""
    x = rd.random()
    if x<0.1 :
        return 4
    else :
        return 2


def grid_add_new_tile_at_position(grille, i, j):
    """Adds a new random tile at position (i,j)"""
    grille[i][j] = get_value_new_tile()
    return grille

def get_all_tiles(grille):
    """Returns the list constituted of all the non-empty tiles"""
    n = len(grille)
    tiles = []
    for i in range(n):
        for j in range(n):
            caractere = grille[i][j]
            if caractere == " ":
                tiles.append(0)
            else:
                tiles.append(caractere)
    return tiles


def get_empty_tiles_positions(grille):
    """Returns a list containing the empty tiles positions (i,j)"""
    n = len(grille)
    empty_tiles=[]
    for i in range(n):
        for j in range(n):
            if grille[i][j] == 0 or grille[i][j] == " ":
                empty_tiles.append((i,j))
    return empty_tiles

def get_new_position(grille):
    """Returns a random empty position and -1 if there is no empty position"""
    empty_tiles = get_empty_tiles_positions(grille)
    if empty_tiles:
        return rd.choice(empty_tiles)
    return -1

def grid_get_value(grille, i, j):
    """Returns the int value of grille[i][j]"""
    valeur = grille[i][j]
    if valeur in [" ", 0]:
        return 0
    else :
        return valeur

def grid_add_new_tile(grille):
    """Add a random tile at a random empty position in grille"""
    x, y = get_new_position(grille)
    return grid_add_new_tile_at_position(grille, x, y)

def init_game(n):
    """Returns a grid with two tiles"""
    grille = create_grid(n)
    for i in range(2):
        grille = grid_add_new_tile(grille)
    return grille

def max_m(m) :
    """Returns the maximum length of the m's elements"""
    n = len(m)
    max = 0
    for i in range(n) :
        for j in range(n) :
                if len(str(m[i][j])) > max :
                    max = len(str(m[i][j]))
    return(max)

def trad(grid,theme) :
    """Returns a new grid with themes elements mapped by m's elements"""
    n = len(grid)
    return([[theme[grid_get_value(grid, i, j)] for j in range(n)] for i in range(n)])

THEMES = {"0": {"name": "Default", 0: "", 2: "2", 4: "4", 8: "8", 16: "16", 32: "32", 64: "64", 128: "128", 256: "256", 512: "512", 1024: "1024", 2048: "2048", 4096: "4096", 8192: "8192"}, "1": {"name": "Chemistry", 0: "", 2: "H", 4: "He", 8: "Li", 16: "Be", 32: "B", 64: "C", 128: "N", 256: "O", 512: "F", 1024: "Ne", 2048: "Na", 4096: "Mg", 8192: "Al"}, "2": {"name": "Alphabet", 0: "", 2: "A", 4: "B", 8: "C", 16: "D", 32: "E", 64: "F", 128: "G", 256: "H", 512: "I", 1024: "J", 2048: "K", 4096: "L", 8192: "M"}}

def grid_to_string_with_size_and_theme(grid, theme, n) :
    """Returns a fancy printable string from a grid with a defined theme"""
    m2 = trad(grid, theme)
    length_max = max_m(m2)
    res = "="+("="+length_max*"=")*n +"\n"
    for i in range(n) :
        for j in range(n) :
            length = len(str(m2[i][j]))
            r = (length_max-length)
            res += ("|" + str(m2[i][j]) + r*" ") #pour ajuster les espaces
            if j == n-1 :
                res += "|"+"\n"
        res += "="+("="+length_max*"=")*n +"\n"
    #print(res)
    return res

def grid_to_string(m, n) :
    """Returns a fancy printable string from a grid with default theme"""
    theme = THEMES["0"]
    return grid_to_string_with_size_and_theme(m,theme, n)

def long_value_with_theme(grid, theme):
    """Returns the maximum length of the m's elements"""
    return max_m(trad(grid, theme))




