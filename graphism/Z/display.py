import tkinter as tk
from tkinter.filedialog import askopenfilenames
from shutil import copyfile
import graphism.Y.graphic_2048 as aff_grille
import os
import Tournament.ia_plays_game as tournoi
import time
os.chdir("..")

def openfile():
    im = askopenfilenames(filetypes=[('python files', '*.py')])
    for name in im :
        print(os.path.basename(name))
        copyfile(name, "IAs/"+os.path.basename(name))



IA_en_lice = tournoi.load_ias()
nb_IA = len(IA_en_lice)
root = tk.Tk()
ajout_des_joueurs = tk.Frame(root, bd=1, relief='solid')
tk.Label(ajout_des_joueurs, text='Les IAs en lice sont : ').grid(row=0, column=0)
if not(IA_en_lice):
    tk.Label(ajout_des_joueurs, text='Aucune IA pour le moment').grid(row=1, column=0)
else :
    i = 0
    for ia in IA_en_lice:
        i+=1
        nom = os.path.basename(ia)
        tk.Label(ajout_des_joueurs, text=nom).grid(row=i, column=0)
ajout_des_joueurs.grid(row=1, column=1)

resultats = tournoi.tournoi()
titre_jeu = tk.Label(text = "IA War", font=("Helvetica", 16))
titre_jeu.grid(row = 0, column = 0)
jeu = aff_grille.graphical_grid_init([[0]*4]*4)
jeu.grid(row = 1, column = 0)

def change():
    for r in resultats:
        titre_jeu.config(text = r[1])
        for grille in r[3]:
            i = 0
            for el in jeu.winfo_children():
                for e in el.winfo_children():
                    for l in e.winfo_children():
                        x = grille[i]
                        l.config(text = str(x if x else " "), bg=aff_grille.TILES_BG_COLOR[x])
                        i+=1
            jeu.update()
            time.sleep(0.1)


ouvrir = tk.Button(ajout_des_joueurs, text="Ajouter des IAs", command=openfile)
ouvrir.grid()

jouer = tk.Button(ajout_des_joueurs, text="Commencer la compétition", command=change)
jouer.grid()


classement = tk.Frame(root)

print(resultats)


for i in range(nb_IA):
    tk.Label(classement, text = "rang "+str(i+1)+" : "+resultats[i][1]+" avec "+str(resultats[i][0])+" points").grid(row=i, column = 0, sticky = "nw")
classement.grid(row= 2, column = 0)



root.mainloop()


