from tkinter import *
import Game.game2048.grid_2048 as game

def graphical_grid_init(grid) :
    principal = Frame()
    list_tiles=[]
    graphical_grid=[]
    for l in grid :
        graphical_grid+=l
    #w=Toplevel()
    #w.title(2048.0)
    #w.grid()
    background = Frame(principal,bg="#C0EDAC",height=400,width=400)
    background.grid()
    for i in range(4) :
        for j in range(4) :
            list_tiles.append(Frame(background,bg=TILES_BG_COLOR[graphical_grid[4*i+j]],height=100,width=100, bd=3, relief=SUNKEN))
            x = graphical_grid[4*i+j]
            Label(list_tiles[-1],width=11,height=5,bg=TILES_BG_COLOR[x],text=str(x if x else " "),font=TILES_FONT).pack(expand=YES)
    for i in range(4) :
        for j in range(4) :
            list_tiles[4*i+j].grid(column=j,row=i)
    return principal


grid_game=game.init_game(4)


TILES_BG_COLOR = {0: "#FCE300", 2: "#E7D000", 4: "#FFCD01", 8: "#F2B305", \
                  16: "#F5A123", 32: "#F69401", 64: "#F39659", \
                  128: "#DF5A01", 256: "#BC4C01", 512: "#F2318B", \
                  1024: "#D20163", 2048: "#D201C4", 4096: "#9D0192", \
                  8192: "#72018E"}

TILES_FG_COLOR = {0: "#776e65", 2: "#776e65", 4: "#776e65", 8: "#f9f6f2", \
                  16: "#f9f6f2", 32: "#f9f6f2", 64: "#f9f6f2", 128: "#f9f6f2", \
                  256: "#f9f6f2", 512: "#f9f6f2", 1024: "#f9f6f2", \
                  2048: "#f9f6f2", 4096: "#f9f6f2", 8192: "#f9f6f2"}

TILES_FONT = {"Verdana", 40, "bold"}



