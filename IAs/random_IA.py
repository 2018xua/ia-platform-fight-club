import random as rd

pseudo = "Gabri Aleatoire"
def play(grid):
    directions = ["g", "h", "d", "b"]
    return rd.choice(directions)
